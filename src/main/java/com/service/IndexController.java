package com.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import java.util.*;

@Service
public class IndexController {

	private static List<String> date = new ArrayList<String>();

	public String getTitleText() {
		return "INDEX CONTROLLER HERE";
	}

	public String getHeaderText() {
		return "Header here";
	}
	
	public String getSubHeaderText() {
		return "Sub header here";
	}
	
	public String getFirstStoryTitle() {
		return "Hello World";
	}
	
	public String getFirstStoryText() {
		return "Hello World";
	}
	
	public String getSecondStoryTitle() {
		return "Hello World";
	}
	
	public String getSecondStoryText() {
		return "Hello World";
	}
	
	public String getThirdStoryTitle() {
		return "Hello World";
	}
	
	public String getThirdStoryText() {
		return "Hello World";
	}

}