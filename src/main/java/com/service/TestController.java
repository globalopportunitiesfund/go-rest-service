package com.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import java.net.*;
import java.io.*;
import java.util.*;


@Service
public class TestController {
	
	private static List<String> dates = new ArrayList<String>();
	private static List<Double> open = new ArrayList<Double>();
	private static List<Double> close = new ArrayList<Double>();
	private static List<String> high = new ArrayList<String>();
	private static List<String> low = new ArrayList<String>();
	private static List<String> volume = new ArrayList<String>();
	private static List<String> adjClose = new ArrayList<String>();
	
	public List<String> getDates() {
		return dates;
	}
	
	public List<Double> getOpenPrices() {
		return open;
	}
	
	public List<Double> getClosePrices() {
		return close;
	}
	
	public String getTitleText() {
		return "TEST CONTROLLER HERE";
	}

	public String getHeaderText() {
		return "Header here";
	}
	
	public String getSubHeaderText() {
		return "Sub header here";
	}
	
	public void getQuote(String assetName) {
		List<String> asset = getAssetFromWeb(assetName);
		makeSenseOfAssetData(asset);
	}
	
	private List<String> getAssetFromWeb(String assetName) {
		List<String> asset = new ArrayList<String>();
		int startYear = 2014;
		int startMonth = 01 - 1;
		int startDay = 01;
		int endYear = 2016;
		int endMonth = 5 - 1;
		int endDay = 15;
		
		try {
			URL url  = new URL("http://ichart.finance.yahoo.com/table.csv?g=d&f=" + endYear + "&e=" + endDay + "&c=" + startYear + "&b=" + startDay + "&a=" + startMonth + "&d=" + endMonth + "&s=" + assetName + "");
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

		    String inputLine;
		    while ((inputLine = in.readLine()) != null) {
		    	asset.add(inputLine);
		    }
		    in.close();	
		} catch (Exception e) {
				System.out.println(e);
		}
		
		return asset;
	}
	
	
	private void makeSenseOfAssetData(List<String> asset) {
		List<String> itemList = new ArrayList<String>();
		
		for (int i=1; i < asset.size(); i++) {
			itemList = Arrays.asList(asset.get(i).split(","));	
			dates.add(itemList.get(0));
			open.add(Double.parseDouble(itemList.get(1)));
			high.add(itemList.get(2));
			low.add(itemList.get(3));
			close.add(Double.parseDouble(itemList.get(4)));
			volume.add(itemList.get(5));
			adjClose.add(itemList.get(6));
		}
	}

}