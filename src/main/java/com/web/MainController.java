package com.web;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.service.ContactController;
import com.service.IndexController;
import com.service.TestController;

@Controller
public class MainController {

	private final Logger logger = LoggerFactory.getLogger(MainController.class);
	private final IndexController indexController;
	private final TestController testController;
	private final ContactController contactController;
	
	final String page_index = "index";
	final String page_test = "test";
	final String page_contact = "contact";

	@Autowired
	public MainController(IndexController indexController, 
						  TestController testController, 
						  ContactController contactController) {
		this.indexController = indexController;
		this.testController = testController;
		this.contactController = contactController;
	}
	
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Map<String, Object> model) {
		String pageLoader = page_index;
		
		model.put("titleText", indexController.getTitleText());
		
		model.put("headerText", indexController.getHeaderText());
		model.put("subHeaderText", indexController.getSubHeaderText());
		
		model.put("firstStoryTitle", indexController.getFirstStoryTitle());
		model.put("firstStoryText", indexController.getFirstStoryText());
		
		model.put("secondStoryTitle", indexController.getSecondStoryTitle());
		model.put("secondStoryText", indexController.getSecondStoryText());
		
		model.put("thirdStoryTitle", indexController.getThirdStoryTitle());
		model.put("thirdStoryText", indexController.getThirdStoryText());
		
		log(pageLoader);
		return pageLoader;
	}
	
	

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String test(Map<String, Object> model) {
		String pageLoader = page_test;
		
		DataSource dataSource = null;
		String sql = "INSERT INTO CUSTOMER " +
				"(CUST_ID, NAME, AGE) VALUES (?, ?, ?)";
		Connection conn = null;

		
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, 1);
			ps.setString(2, "TestName");
			ps.setInt(3, 25);
			ps.executeUpdate();
			ps.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
			
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
		



		model.put("titleText", testController.getTitleText());
		model.put("headerText", testController.getHeaderText());
		model.put("subHeaderText", testController.getSubHeaderText());
		
		testController.getQuote("AAPL");
		
		model.put("dates", testController.getDates());
		model.put("openPrices", testController.getOpenPrices());
		model.put("closePrices", testController.getClosePrices());
		
		log(pageLoader);
		return pageLoader;
	}
	
	
	@RequestMapping(value = "/contact", method = RequestMethod.GET)
	public String contact(Map<String, Object> model) {
		String pageLoader = page_contact;
		
		model.put("titleText", contactController.getTitleText());
		
		model.put("headerText", contactController.getHeaderText());
		model.put("subHeaderText", contactController.getSubHeaderText());
		
		model.put("firstStoryTitle", contactController.getFirstStoryTitle());
		model.put("firstStoryText", contactController.getFirstStoryText());
		
		model.put("secondStoryTitle", contactController.getSecondStoryTitle());
		model.put("secondStoryText", contactController.getSecondStoryText());
		
		model.put("thirdStoryTitle", contactController.getThirdStoryTitle());
		model.put("thirdStoryText", contactController.getThirdStoryText());
		
		log(pageLoader);
		return pageLoader;
	}
	
	
	public void log(String methodName) {
		logger.debug(methodName.concat("() is executed!"));
	}
	
}