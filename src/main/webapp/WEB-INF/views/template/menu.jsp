<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<body>  
    <div class="container">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Global Opprortunities</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        
                        <c:choose>
                            <c:when test="${requestScope['javax.servlet.forward.request_uri']=='/'}">
                                <li class="active"><a href="/">Home</a></li>
                            </c:when>
                            <c:otherwise>
                                <li><a href="/">Home</a></li>
                            </c:otherwise>
                        </c:choose>
                            
                        <c:choose>
                            <c:when test="${requestScope['javax.servlet.forward.request_uri']=='/test'}">
                                <li class="active"><a href="/test">Test</a></li>
                            </c:when>
                            <c:otherwise>
                                <li><a href="/test">Test</a></li>
                            </c:otherwise>
                        </c:choose>
                        
                        <c:choose>
                            <c:when test="${requestScope['javax.servlet.forward.request_uri']=='/contact'}">
                                <li class="active"><a href="/contact">Contact</a></li>
                            </c:when>
                            <c:otherwise>
                                <li><a href="/contact">Contact</a></li>
                            </c:otherwise>
                        </c:choose>
                        
                    </ul>
                </div>
            </div>	
        </nav>
    </div>
</body>
</html>
    