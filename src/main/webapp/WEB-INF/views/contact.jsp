<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache"> 
	<meta http-equiv="Cache-Control" content="no-cache"> 
	<meta http-equiv="Expires" content="Sat, 01 Dec 2012 00:00:00 GMT">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="THJ Blog" />
	<meta name="author" content="THJ Blog" />
	
	<!-- Include the css files -->
	<jsp:include page="template/included.jsp"/>
	
	<title>${titleText}</title>
</head>

<body>
	<!-- Include the menu -->
	<jsp:include page="template/menu.jsp"/>
	
	<!-- Do stuff -->
	<div class="jumbotron">
		<div class="container">
			<h1>${headerText}</h1>
			<p>
				<c:if test="${not empty subHeaderText}">
					${subHeaderText}
				</c:if>
				<c:if test="${empty subHeaderText}">
					Welcome!
				</c:if>
			<p>
				<a class="btn btn-primary btn-lg" href="/" role="button">Learn more</a>
			</p>
		</div>
	</div>

	<!-- Arrange the containers -->
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h2>${firstStoryTitle}</h2>
				<p>${firstStoryText}</p>
				<p>
					<a class="btn btn-default" href="#" role="button">View details</a>
				</p>
			</div>
			
			<div class="col-md-4">
				<h2>${secondStoryTitle}</h2>
				<p>${secondStoryText}</p>
				<p>
					<a class="btn btn-default" href="#" role="button">View details</a>
				</p>
			</div>
			
			<div class="col-md-4">
				<h2>${thirdStoryTitle}</h2>
				<p>${thirdStoryText}</p>
				<p>
					<a class="btn btn-default" href="#" role="button">View details</a>
				</p>
			</div>
		</div>
		<hr>
				
		<!-- Include the footer -->
		<jsp:include page="template/footer.jsp"/>		
	</div>
	
	<!-- Bootstrap core JavaScript -->
	<spring:url value="/resources/core/css/hello.js" var="coreJs" />
	<spring:url value="/resources/core/css/bootstrap.min.js" var="bootstrapJs" />
	<script src="${coreJs}"></script>
	<script src="${bootstrapJs}"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</body>
</html>